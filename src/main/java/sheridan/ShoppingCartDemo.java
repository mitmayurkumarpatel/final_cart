package sheridan;

/**
 *
 * @author mitpatel
 */
public class ShoppingCartDemo {
    
    public static void main( String args [ ] ) {
        PaymentServiceFactory factory = PaymentServiceFactory.getInstance( );
        PaymentService creditService = factory.getPaymentService( PaymentServiceType.CREDIT );
        PaymentService debitService = factory.getPaymentService( PaymentServiceType.DEBIT );        
            // create cart and add products
        Cart cart = new Cart( );
        cart.addProduct( new Product( "shirt" , 50 ) );
        cart.addProduct( new Product( "pants" , 60 ) );
            // set credit service and pay
        cart.setPaymentService( creditService );        
        cart.payCart();
            // set debit service and pay
        cart.setPaymentService( debitService );    
        cart.payCart();  
        
        //getting size of cart
        System.out.println("SIZE OF PRODUCT " +cart.getCartSize());
        
        //printing discount in amount
        DiscountByAmount a1 = new DiscountByAmount();
        System.out.println("Amount with discount = " + a1.calculateDiscount(500.25));
        
        //printing amount in percentage
        DiscountByPercentage p1 = new DiscountByPercentage();
       System.out.println("Amount Percetage with discount = " +p1.calculateDiscount(500.25)); 
    } 
    
}
