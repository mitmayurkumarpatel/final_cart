/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package sheridan;

/**
 *
 * @author mitpa
 */
public class ShoppingCartFactory 
{
    private static ShoppingCartFactory factory;
    
    private ShoppingCartFactory()
    {}
    
    public static ShoppingCartFactory getInstance()
    {
        if(factory==null)
            factory = new ShoppingCartFactory();
        return factory;
    }
    
    //enum
    public Discount getDiscount(DiscountTypes types)
    {
        switch(types)
        {
           case DISCOUNTBYAMOUNT : return new DiscountByAmount();
        case DISCOUNTBYPERCENTAGE : return new DiscountByPercentage();
        }
       return null;
    }
    
}
