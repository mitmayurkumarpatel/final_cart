/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package sheridan;

/**
 *
 * @author mitpa
 */
public class DiscountByPercentage extends Discount 
{
    public double discountPercentage = 5;
    public double discount;

    public double getDiscount() {
        return discount;
    }
    
    public double getDiscountPercentage() {
        return discountPercentage;
    }
    

    @Override
    public double calculateDiscount(double amount) 
    {
        double discount = amount * discountPercentage / 100;
        return discount;
    }
    
    
}
