package sheridan;

/**
 *
 * @author mitpatel
 */
public enum PaymentServiceType {
    CREDIT, DEBIT;
}
