package sheridan;

/**
 *
 * @author mitpatel
 */
public abstract class PaymentService {
    
    public abstract void processPayment( double amount );
}
