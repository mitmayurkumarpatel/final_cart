package sheridan;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mitpatel
 */
public class Cart {
    
    private List <Product> products = new ArrayList<>( );
    private PaymentService service;
    
    public void setPaymentService( PaymentService service ) {
        this.service  = service;
    }

    public int getCartSize()
    {
        return getProducts().size();
    }
    public List<Product> getProducts() {
        return products;
    }

    public void addProduct( Product product ) {
        products.add( product );
    }
    
    public void payCart( ) {
        double total = 0;
        
       for ( Product product : products ) {
           total += product.getPrice( );
       }
        service.processPayment( total );
    }
}
