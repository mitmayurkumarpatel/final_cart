/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package sheridan;

import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author mitpa
 */
public class CartTest {
    
    public CartTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }
    private Cart cart = new Cart();
    
    private Product product1 = new Product("shirt", 50);
    private Product product2 = new Product("pants", 100);

    public Product getProduct1() {
        return product1;
    }

    public Product getProduct2() {
        return product2;
    }

   
    @Test
    public void testAddProductIsValid() {
        System.out.println("Test IsValid");
       cart.addProduct(product1);
        assertEquals(1, cart.getCartSize());
    }
    
    @Test
    public void testAddProductIsInvalid() {
        System.out.println("Test is InValid");
         cart.addProduct(product2);
        assertEquals(1, cart.getCartSize());
        
    }

    
}
